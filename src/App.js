import './App.css';
import imgFront from './img/HTML_Monochromatic.svg';
import imgJs from './img/Javascript_img.svg';
import imgMe from './img/imgMe.png';
import funcionesApp from './funciones/funcionesApp';
import listenerFunciones from './funciones/listenersFunciones';

function App() {
  

  return (
    <div className="App">
      <header className="App-header">
        <div className="barMenu">
          <div className="square-left" onClick={()=>{funcionesApp.leftSquareClick()}}>
            <div className="menu-select"></div>
          </div>
          <div className="menu-all" id="menuAll">
            <div className="menu-left-content">
              <div className="close-button" onClick={()=>{funcionesApp.closeLeftMenu()}}></div>
              <ul className="list-left">
                <h4>Codigo utilizado</h4>
                <li>
                  HTML5
                </li>
                <li>
                  CSS3
                </li>
                <li>
                  JavaScript
                </li>
                <li>
                  NodeJS
                </li>
                <li>
                  React
                </li>
                <li>
                  Git
                </li>
              </ul>
            </div>
          </div>
          <div className="text-bar">
            Portafolio
          </div>
          <div className="text-contact" id="contactElement">
            Contact
          </div>
          <div className="info-menu" id="infoMenu">
              <div className="info-menu-text" id="infoMenuText">
                <ul>
                  <li>Nombre: Cristian Medina</li>
                  <li>Tel: +541161983672</li>
                  <li id="email-info">Email: cristianleonardo231197@gmail.com</li>
                  <li><a href="https://www.linkedin.com/in/cristian-medina-745445220/"> Linkedin profile</a></li>
                </ul>
            </div> 
          </div>
        </div>
      

      <div>
        <div className="tittleApp" id="tittlePage">
          <h2 className="tittleText">Hola,</h2>
          <h2 className="subTittleText">me llamo</h2>
          <h3 className="nameCristian">Cristian</h3> 
          <img src={imgFront} className="front-img"/>
          <img src={imgJs} className="front-img-js"/>
        </div>

        <div className="firstMenu" onClick={()=>{funcionesApp.firstClick()}}>
          <h5 className="dataSelect">About</h5>
        </div>
        <div className="firstMenu-space" id="first-Space">
          <div className="left-Div">
            <div className="animationAD"></div>
            <div className="animationAD1"></div>
            <div className="animationAD2"></div>
            <div className="animationAD3"></div>
            <div className="presentation-Text">
              <h5 className="tittle-about">ABOUT ME</h5>
              <h6>Desarrollador Full Stack Jr. muy autodidacta con conocimientos en diseño audiovisual y con flexibilidad ante los cambios.
Puedo desarrollar trabajo en equipo (Scrum) o en solitario. 
Actualmente me encuentro en la búsqueda de un empleo que me permita demostrar mis capacidades y descubrir nuevas tecnologías que me ayuden a desarrollarme en mi carrera profesional.
              </h6>
              <div className="square-wrapper">
                <div className="square square-transition"></div>
              </div>
            </div> 
          </div>
          <div className="page-detail">
              <div className="page-detail-text">
                <h5 className="tittle-about">THIS PAGE</h5>
                <h6>Esta pagina contiene un vistazo de mis habilidades con:
                  <ul>
                    <li>.HTML5 y sus elementos.</li>
                    <li>.CSS3 y su uso de animaciones y separaciones.</li>
                    <li>.JavaScript y sus diferentes interacciones con la propia pagina.</li>
                    <li>.React y NodeJS.</li>
                    <li>.Este no se trata de un proyecto final, sino de una pagina que se actualiza segun adquiero el conocimiento para completar diferentes aspectos de la misma.</li>
                  </ul>
                </h6>
              </div>
          </div>


          <div className="right-Div1">
              
          </div>
          <div className="right-Div2">
              
          </div>
          <div className="right-Div3">
              
          </div>
          <div className="img-me">
            <img src={imgMe} className="img-me-img"/>
          </div>
        </div>

        <div className="secondMenu" onClick={()=>{funcionesApp.secondClick()}}>
          <h5 className="dataSelect">Studies</h5>
        </div>
        <div className="secondMenu-space" id="second-Space">
            <div className="galeria-courses">
              
            </div>
        </div>

        <div className="thirdMenu" onClick={()=>{funcionesApp.thirdClick()}}>
          <h5 className="dataSelect">Works</h5> 
        </div>
        <div className="thirdMenu-space" id="third-Space">
            
        </div>

      </div>
      </header>
    </div>
  );
}

export default App;
