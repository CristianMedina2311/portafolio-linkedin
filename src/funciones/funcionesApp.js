import App from "../App";

const funcionesApp = {
    firstClick : () => {
        var firstSpace = document.getElementById('first-Space');
        var secondSpace = document.getElementById('second-Space');
        var thirdSpace = document.getElementById('third-Space');

        firstSpace.style.cssText = 'z-index: 2;';
        secondSpace.style.cssText = 'z-index: 1;';
        thirdSpace.style.cssText = 'z-index: 1;';
    },
    secondClick: () => {
        var firstSpace = document.getElementById('first-Space');
        var secondSpace = document.getElementById('second-Space');
        var thirdSpace = document.getElementById('third-Space');

        firstSpace.style.cssText = 'z-index: 1;';
        secondSpace.style.cssText = 'z-index: 2;';
        thirdSpace.style.cssText = 'z-index: 1;';
    },
    thirdClick: () => {
        var firstSpace = document.getElementById('first-Space');
        var secondSpace = document.getElementById('second-Space');
        var thirdSpace = document.getElementById('third-Space');

        firstSpace.style.cssText = 'z-index: 1;';
        secondSpace.style.cssText = 'z-index: 1;';
        thirdSpace.style.cssText = 'z-index: 2;';

    },
    leftSquareClick: () => {
        var divAll = document.getElementById('menuAll');
             
        divAll.style.cssText = 'transform: translateX(1%);';
    },
    closeLeftMenu: ()=> {
        var divAll = document.getElementById('menuAll');
             
        divAll.style.cssText = 'transform: translateX(-150%)';
    },
    apearMenuTop: ()=>{
        var getContact = document.getElementById('contactElement');
        var getMenu = document.getElementById('infoMenu')
        var getContent = document.getElementById('infoMenuText');
        getContact.addEventListener('mouseover', function(){
            getMenu.style.cssText = 'opacity: 1; height: 200px; transform: translateY(0);';
            getContent.style.cssText = 'opacity: 1; transform: translateY(0);';
        })
        getMenu.addEventListener('mouseover', function(){
            getMenu.style.cssText = 'opacity: 1; height: 200px;transform: translateY(0);';
            getContent.style.cssText = 'opacity: 1; transform: translateY(0);';
        })

        getMenu.addEventListener('mouseout', function(){
            getMenu.style.cssText = 'opacity: 0; height: -150%;';
            getContent.style.cssText = 'opacity: 0; transform: translateY(-150%);';
        })
        getContact.addEventListener('mouseout',function(){
            getMenu.style.cssText = 'opacity: 0.0; height: -150%;';
            getContent.style.cssText = 'opacity: 0; transform: translateY(-150%);';
        })
    }
}

export default funcionesApp;